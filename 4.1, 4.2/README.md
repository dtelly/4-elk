# 사전 세팅

큰 데이터를 다루고 있지 않기 때문에 오케스트레이션까지는 필요가 없을 듯 하여 편의상 로컬에 `docker`를 설치하고 `docker-compose`를 통해 환경을 구성하기로 했다.
- [OS별 docker/docker-compose 설치](https://docs.docker.com/compose/install/)

`docker-compose.yml`에서 책과 같은 `6.4.3` 버전의 elasticsearch 와 kibana를 세팅하도록 작성했다. 따라서 `git clone`을 하거나 [이 폴더 zip 파일](https://gitlab.com/dtelly/4-elk/-/archive/master/4-elk-master.zip?path=4.1%2C+4.2)을 받아 압축 해제한 뒤, 이 폴더에서 bash를 열고
```bash
$ docker-compose up
```
을 통해 컨테이너들을 띄우고,
```bash
$ docker-compose down
```
으로 컨테이너들을 내린다.

책에서 제시한 `elasticsearch.yml` 및 snapshot들은 `docker-compose.yml`에서 volume으로 연결했기 때문에 elasticsearch 컨테이너에서 스냅샷을 복구할 수 있도록 하였다.

## 참고사항
아래의 코드들은 책에서 제공하는 GitHub repository의 [예제 코드](https://github.com/javacafe-project/elastic-book/blob/master/document/sample/chapter04)에서 대부분 참고했다.

# 4.1 검색 API

이 장에서는 색인된 문서를 검색하는 여러 검색 API에 대해서 다룬다.
엘라스틱서치는 데이터를 색인 시점에 텀으로 분리해서 아래와 같이 역색인 구조로 만든다.
이후에 검색할 때 이 역색인을 참조해서 해당하는 문서들을 찾아낼 수 있게 된다.
* 검색 대상 타입
  * Text: terms로 쪼개짐.
  * Keyword: terms로 쪼개지지 않음.

![](image/4.1.png)

색인 시점에도 분석기를 통해 데이터를 terms로 쪼개지만, 검색 시점에도 마찬가지로 분석기를 사용하여 검색어를 terms로 쪼개서 스코어를 계산한다.

직접 검색 API를 실습하기 위해 미리 준비된 snapshot을 복구하는 과정이 필요하다.

## Snapshot 사용하기

### Snapshot 위치 지정

터미널을 열어서 `docker-compose up`으로 es, kibana를 실행해주고, 아래와 같이 `curl`로 http request를 날려 es에게 snapshot의 위치를 알려줄 수 있다.
여기서 지정된 `location`은 실질적으로 `docker-compose.yml`의 `volume`을 통해 `volume/snapshot`으로 연결되어있기 때문에 해당 폴더를 사용하게 된다.

```bash
curl -H 'Content-Type: application/json' \
     -XPUT 'http://localhost:9200/_snapshot/javacafe' \
     -d '{"type": "fs", "settings": {"location": "/es/book_backup/search_example", "compress": true}}' 
```
이렇게 `curl`을 통해서 es에 직접 접근하여 요청을 내릴수도 있지만, 어차피 kibana도 올라가있는 상태이므로 kibana의 Dev Tools 를 사용하면 더 편리하게 실습할 수 있다.

[localhost kibana DevTools](http://localhost:5601/app/kibana#/dev_tools/console?_g=()) 에 들어가서 아래를 입력해도 동일하게 동작한다.


```json
PUT _snapshot/javacafe
{
  "type": "fs",
  "settings": {
    "location": "/es/book_backup/search_example",
    "compress": true
  }
} 
```

`curl`을 사용할 때는 `-H`로 헤더도 넣어야 하고, 주소 및 포트도 넣어줘야 하는 등 더 지저분하기 때문에 앞으로는 DevTools 명령어를 기준으로 예시를 작성한다.

### 올라간 스냅샷 확인
```
GET _snapshot/javacafe/_all
```
`movie-search`라는 스냅샷이 있는 것을 확인할 수 있다.

### 스냅샷 사용
이번 장에서는 `movie-search`스냅샷을 사용하므로 아래와 같이 스냅샷 복구를 해준다.
```
POST _snapshot/javacafe/movie-search/_restore
```

복구된 내용에 대한 요약을 아래 명령어를 통해 확인해볼 수 있다.
```
GET /_cat/indices/movie_search?v&pretty
```
돌려보면 63,069개의 문서가 있는 것을 확인할 수 있다.

## 4.1.1 검색 질의 표현 방식
검색 API는 기본적으로 query 기반이며, 두가지 방식으로 표현할 수 있다.
* URI 검색
  * 루씬에서 사용하던 전통적인 방법
* Request body 검색
  * RESTful API의 request body 사용

### URI 검색 예시

아래와 같은 [URI 형식](https://ko.wikipedia.org/wiki/%ED%86%B5%ED%95%A9_%EC%9E%90%EC%9B%90_%EC%8B%9D%EB%B3%84%EC%9E%90)을 사용한 방식이다.
```
scheme:[//[user[:password]@]host[:port]][/path][?query][#fragment]
```

`curl`로 직접 es에 접근하는 대신 kibana dev tools를 통해 접근할 때는 위와 같은 URI 형식에서 `scheme` ~ `port`까지 kibana가 알아서 연결되어있는 es로 지정해주므로, kibana dev tools에서는 `path`와 `query`만 작성해서 실행해주면 된다.

Dev tools에서 아래와 같이 query를 통해 제작년도가 2018년인 영화들을 뽑아낼 수 있다.

```
GET movie_search/_search?q=prdtYear:2018
```

### Request body 검색 예시
위에서 `q`키를 통해 작성한 쿼리를 request body에 풀어서 쓰는 방식이다.
`scheme` ~ `port`까지 kibana가 알아서 연결해준 것처럼, 중괄호 묶음을 kibana가 알아서 request body에 담아서 es에게 날려준다.

```json
POST movie_search/_search
{
  "query":{
    "term":{"prdtYear": "2018"}
  }
}
```

## 4.1.2 URI 검색

앞서의 예시에서 나왔던 것처럼 `q`파라미터를 사용해서 아래와 같이 영문제목에 `Family`가 포함된 영화들을 찾을 수 있다.
```
POST movie_search/_search?q=movieNmEn:Family
```
그 밖에도 아래와 같이 다양한 파라미터들이 있다.
![](image/table4.1.png)

* 장점
  * 길이가 짧다.
* 단점
  * 가독성이 현저히 떨어진다.
  * 복잡한 식을 작성하기 어렵다.

```
POST movie_search/_search?q=movieNmEn:* AND prdtYear:2017&analyze_wildcard=true&from=0&size=5&sort=_score:desc,movieCd:asc&_source_includes=movieCd,movieNm,mvoieNmEn,typeNm&pretty
```

## Request body 검색

위에서 본 것처럼 URI 방식은 지저분하기 때문에, 보통은 request body에 json을 담아서 보내는 방식을 사용한다.

```json
POST movie_search/_search
{
  "query": {
    "query_string": {
      "default_field": "movieNmEn",
      "query": "Family"
    }
  }
}
```

엄밀히 말하자면 임의의 json을 담아서 보내는 것은 아니고, json을 기반으로 한 [Query DSL 문법 구조](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html)에 맞는 문장을 담아서 보내야 es가 제대로 인식할 수 있다.

아래는 URI 방식에서 보냈던 복잡한 쿼리를 더 보기 편한 Query DSL 방식에 맞추어 작성한 것이다.

```json
POST movie_search/_search
{
  "query": {
    "query_string": {
      "default_field": "movieNmEn",
      "query": "movieNmEn:* OR prdtYear:2017"
    }
  },
  "from": 0,
  "size": 5,
  "sort": [
    {
      "_score": {
        "order": "desc"
      },
      "movieCd": {
        "order": "asc"
      }
    }
  ],
  "_source": [
    "movieCd",
    "movieNm",
    "mvoieNmEn",
    "typeNm"
  ]
}
```

# 4.2 Query DSL 이해하기
위에서도 언급했던 [Query DSL 공식 문서](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html)에 더 자세한 내용이 나와있지만, 기본적인 Query DSL의 요청/응답문의 구조는 아래와 같다.

## 4.2.1 Query DSL의 구조
### 요청 구조
```json5
{
  "size": ...,       // 리턴받을 결과 수(기본값 10)
  "from": ...,       // 검색결과 중 몇번째 문서부터 가져올지
  "timeout": ...,    // 최대 검색 시간

  "_source": { },    // 검색 결과에 포함할 필드
  "query": { },      // 검색 조건문
  "aggs": { },       // 통계 / 집계
  "sort": { }        // 검색 결과 정렬
}
```

### 응답 구조
```json5
{
  "took": ...,           // 쿼리 수행에 걸린 시간

  "timed_out": ...,      // 쿼리 시간 초과 여부

  "_shards": {
    "total": ...,        // 쿼리를 요청한 샤드의 수
    "successful": ...,   // 요청에 성공적으로 응답한 샤드 수
    "failed": ...        // 요청에 실패한 샤드 수
  },

  "hits": {
    "total": ...,        // 검색어와 매칭된 문서 수
    "max_score": ...,    // 검색된 문서 중 가장 큰 스코어
    "hits": [ ]          // 각 문서의 내용 및 메타 정보
  }
}
```

## 4.2.2 쿼리 / 필터 컨텍스트

* 쿼리 컨텍스트
  * 전문 검색
  * 분석기에 의한 분석 수행
  * 연관성 스코어 계산
  * 루씬 레벨의 분석 과정
  * 상대적으로 느림
```json5
POST  movie_search/_search
{
  "query": {        // 쿼리 컨텍스트
    "match": {
      "movieNm": "기묘한 가족"
    }
  }
}
```

* 필터 컨텍스트
  * 조건 검색
  * yes/no 로 단순 판별 가능
  * 연관성 스코어 계산 안함.
  * 자주 사용하는 필터는 내부적으로 캐싱
  * 상대적으로 빠름
  * 예: 장르가 `다큐멘터리`인가?

```json5
POST movie_search/_search
{
  "query": {       // 쿼리 컨텍스트
    "bool": {
      "must": [
        {
          "match_all": {}
        }
      ],
      "filter": {      // 필터 컨텍스트
        "term": {
          "repGenreNm": "다큐멘터리"
        }
      }
    }
  }
}
```

## 4.2.3 Query DSL 주요 파라미터

### Multi index 검색
아래와 같이 `movie_search`와 `movie_auto`각각이 포함하고 있는 필드인 `repGenreNm`에 대한 검색 가능.
```json
POST movie_search,movie_auto/_search
{
  "query": {
    "term": {
      "repGenreNm": "다큐멘터리"
    }
  }
}
```

아래와 같이 와일드카드를 사용해서 `log-2019-`로 시작하는 모든 인텍스에 대해서 검색 가능
```
POST log-2019-*/_search
```

### 쿼리 결과 페이징
다음과 같이 0번부터 5개 항목을 가져올 수 있다.
```json
POST movie_search/_search
{
   "from": 0,
   "size": 5,
   "query": {
     "term": {
     "repNationNm": "한국"
     }
   }
}
```

또한 다음과 같이 5번부터 5개항목을 가져올 수 있다.
하지만 내부적으로는 여전히 0번부터 찾게 되므로 `from`이 높을수록 오래걸린다.
```json
POST movie_search/_search
{
   "from": 5,
   "size": 5,
   "query": {
     "term": {
     "repNationNm": "한국"
     }
   }
}
```

### 쿼리결과 정렬
* 기본적으로는 유사도 스코어 값을 정렬.
* 다른 순서로 재정렬하고 싶을때 `sort`파라미터를 통해서 각 필드에 대한 `order`를 `asc`/`desc`로 지정하여 정렬할 수 있다.

아래는 정렬과 2단계 정렬 예시.
```json
POST movie_search/_search
{
   "query": {
     "term": {
       "repNationNm" : "한국"
     }
   },
   "sort" :{
     "prdtYear": {
        "order": "asc"
      }
   }
}

POST movie_search/_search
{
  "query": {
    "term": {
      "repNationNm" : "한국"
    }
  },
  "sort" :{
    "prdtYear": {
      "order": "asc"
    },
    "_score": {
      "order": "desc"
    }
  }
}
```

### 필드 필터링(`_source`)

* `_source` 파라미터를 통해 원하는 필드만 볼 수 있다.
* 불필요한 네트워크 사용량을 줄임.

```json
POST movie_search/_search
{
   "_source": [
     "movieNm"
   ],
   "query": {
     "term": {
       "repNationNm" : "한국"
     }
   }
}
```

### 범위 검색
* lt(<)
* lte(<=)
* gt(>)
* gte(>=)

제작년도가 `>= 2016 이고, <= 2017`인 데이터 검색
```json
POST movie_search/_search
{
  "query": {
    "range": {
      "prdtYear": {
        "gte": "2016",
        "lte": "2017"
      }
    }
  }
}
```

### operator 설정

`match`는 기본적으로 각 텀을 `OR`연산으로 찾지만, 아래와 같이 `operator`파라미터에 명시해서 `자전차왕`과 `엄복동`이 모두 포함된 문서를 찾는다. 

```json
POST movie_search/_search
{
  "query": {
    "match": {
      "movieNm": {
        "query": "자전차왕 엄복동",
        "operator": "and"
      }
    }
  }
}
```

### minimum_should_match 설정
* 최소 몇 개의 텀이 포함되어야 하는지 명시
```json
POST movie_search/_search
{
  "query": {
    "match": {
      "movieNm": {
        "query": "자전차왕 엄복동",
        "minimum_should_match": 2
      }
    }
  }
}
```

### fuzziness 설정
* 약간의 오타가 있어도(유사한 값을 가져도) 검색 결과에 포함
  * [레벤슈타인 편집 거리 알고리즘](https://dzone.com/articles/the-levenshtein-algorithm-1) 기반
  * `0, 1, 2, AUTO`의 4가지 옵션
  * 한글보다는 알파벳에 더 유용

```json
POST movie_search/_search
{
  "query": {
    "match": {
      "movieNmEn": {
        "query": "Fli High",
        "fuzziness": 1
      }
    }
  }
}
```

### boost 설정
* 원하는 필드/키워드에 더 많은 가중치를 준다.

아래 예시는 `movieNm`에 3배의 가중치를 준다.
```json
POST movie_search/_search
{
  "query" : {
    "multi_match": {
      "query": "Fly",
      "fields": ["movieNm^3","movieNmEn"]
    }
  }
}

```
