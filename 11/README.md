# 장애 방지를 위한 실시간 모니터링

# 사전 세팅

`git clone` 또는 `zip파일 다운로드`후에 상위폴더의 4.1, 4.2 장 폴더로 가서 `docker-compose up`으로 docker 를 띄우고, snapshot을 복구한다.

- 해당 docker-compose.yml 내의 포트 설정
  - Elasticsearch
    - http://localhost:9200
  - Kibana
    - http://localhost:5601

## kibana 를 통한 간략한 health check

[{kibana}/app/monitoring](http://localhost:5601/app/monitoring)

# 11.1 클러스터 Health 체크

## 11.1.1 Health 체크: cluster

[{elasticsearch}/_cluster/health](http://localhost:9200/_cluster/health)

- 클러스터 레벨의 간단한 요약정보
- cluster_name
  - 클러스터의 이름
- status
  - 클러스터 상테 표시. 가장 기본적으로 확인해야 할 항목.
  - green: 모든 샤드 정상
  - yellow: 모든 프라이머리 샤드 정상, 일부 레플리카 샤드 이상
  - red: 일부 프라이머리 샤드 이상
- timed_out
  - API 타임아웃 여부
- number_of_nodes
  - 엘라스틱 서치 노드 수
- number_of_data_nodes
  - 노드 중 데이터 노드 수
- active_primary_shards
  - 클러스터 중 프라이머리 사드 수
- relocating_shards
  - 복구를 위해 relocation 작업중인 샤드 수. 정상값 0
- initializing_shards
  - 초기화 진행중인 샤드
  - 오랫동안 초기화중이라면 문제가 있는 것
- unassigned_shards
  - 할당되지 않은 샤드 수. 평상시 대부분 0값

## 11.1.2 Health 체크: index

[{elasticsearch}/_cluster/health?level=indices](http://localhost:9200/_cluster/health?level=indices)

- level 파라미터: indices
- 모든 인덱스 정보가 포함되어 리턴
  - 좀 더 자세한 정보 표현
  - 문제있는 인덱스 찾기 편함

## 11.1.3 Health 체크: shard

[{elasticsearch}/_cluster/health?level=shards](http://localhost:9200/_cluster/health?level=shards)

- level 파라미터: shards
- 인덱스 내부의 샤드 레벨까지 포함
  - 더욱 자세한 정보

## 11.1.4 Health 체크 활용

### 특정 인덱스 대상

[{elasticsearch}/_cluster/health/movie_search](http://localhost:9200/_cluster/health/movie_search)

- `active_primary_shards`, `active_shards` 등의 값이 해당 인덱스에 있는 샤드들을 기준으로 리턴.

### 특정 인덱스, 인덱스 레벨까지 표현

[{elasticsearch}/_cluster/health/movie_search?level=indices](http://localhost:9200/_cluster/health/movie_search?level=indices)

### 특정 인덱스, 샤드 레벨까지 표현

[{elasticsearch}/_cluster/health/movie_search?level=shards](http://localhost:9200/_cluster/health/movie_search?level=shards)

# 11.2 물리적인 클러스터 상태 정보 조회

- 엘라스틱서치 실행 시
  - config 디렉터리에 설정된 환경설정 정보
    - 노드 인스턴스 생성
- `_cluster/state` API 이용하여 확인
  - 클러스터 환경설정
  - 노드 라우팅 방식
  - 인덱스 매핑 구성

## 물리 상태 조회: 클러스터 레벨

[{elasticsearch}/_cluster/state](http://localhost:9200/_cluster/state)

- metadata 정보
- routing_table 정보
- restore/snapshot 정보

## 물리 상태 조회: 노드 레벨

[{elasticsearch}/_nodes](http://localhost:9200/_nodes)

- 엘라스틱서치 노드가 실행될 때 어떤 설정으로 생성되었는지 확인 가능
- 많은 정보가 결과로 제공되기 때문에 필터링 조건 제공
- ip 별 노드 정보
  - [{elasticsearch}/_nodes/[ip]](http://localhost:9200/_nodes/[ip])
- 특정 id 노드 정보
  - [{elasticsearch}/_nodes/[id]](http://localhost:9200/_nodes/[id])
- 호출받은 노드 상태
  - [{elasticsearch}/_nodes/_local](http://localhost:9200/_nodes/_local)
- 표현정보
  - settings
    - `elasticsearch.yml` 에 설정된 기본 설정사항
    - yml 파일을 수정하고 변경사항이 반영되었는지 확인 가능
  - os
    - 엘라스틱서치 인스턴스가 실행된 운영체게 정보
  - process
    - es 인스턴스가 생성될때 memory lock이 수행되었는지 여부
  - jvm
    - 엘라스틱서지 인스턴스가 생성될때의 JVM 옵션
  - thread_pool
    - 인스턴스가 생성될 때의 쓰레드풀 관련 정책
  - transport
    - 엘라스틱서치는 클러스터 `내부 노드들간의 통신`으로 transport 모듈 이용
    - 어떤 포트들이 바인딩 되었는지 확인
  - http
    - 엘라스틱서치는 `외부 통신`을 위해 http 모듈 이용
    - 어떤 포트들이 바인딩 되었는지 확인
  - plugins
    - 엘라스틱서치의 플러그인 확장 지원
    - 어떤 플러그인들이 설치되어있는지 확인
  - modules
    - 내부에 다양한 모듈들로 구성
    - 어떤 모듈들이 동작중인지 확인


# 11.3 클러스터에 대한 실시간 모니터링

- 엘라스틱서지의 안정적인 클러스터 운영
  - 평상시에 지속적인 클러스터 상태 확인
  - 물리적으로 다수의 서버로 구성
    - 다양한 관점에서 모니터링해야 함
  - 클러스터 -> 다수의 노드
  - 노드 -> 다수의 사드
  - 물리적인 샤드 -> 논리적인 인덱스 구성
  - 인덱스들 -> 클러스터
    - 샤드레벨부터 살펴볼 수 있도록 API제공

## 실시간 모니터링: 클러스터 레벨

[{elasticsearch}/_cluster/stats](http://localhost:9200/_cluster/stats)

- indices 속성
  - count
    - 클러스터에 존재하는 인덱스 수
  - shards
    - 클러스터에 존재하는 샤드 정보
  - docs
    - 클러스터에 색인된 문서 정보
  - store
    - 클러스터의 문서들이 차지하는 디스크 크기
  - fielddata
    - 클러스터가 fielddata를 위해 생성한 메모리 크기
  - query_cache
    - 빠른 검색결과를 위해 캐싱을 사용하는데, 이에 대한 메모리 공간 및 hit/miss 정보
  - completion
    - 자동완성을 위한 메모리 크기
  - segments
    - 클러스터 내부의 루씬 세그먼트가 사용중인 메모리 정보
- node 속성
  - count
    - 클러스터에 존재하는 노드 수
  - versions
    - 엘라스틱서치 버전
  - os
    - 클러스터에 존재하는 노드의 운영체제 정보
  - process
    - 노드의 CPU사용 정보
  - jvm
    - 노드의 jvm 정보
  - fs
    - 노드의 file system 사용량 정보
  - plugins
    - 노드에 설치된 플러그인 정보
  - network_types
    - 노드에서 사용중인 네트워크 타입 정보

## 실시간 모니터링: 노드 레벨

[{elasticsearch}/_nodes/stats](http://localhost:9200/_nodes/stats)

- 물리적인 노드의 역할, 통계 정보 등을 상세히 보여줌
- 각 노드 정보를 비교하면 문제가 발생한 노드를 찾을 수 있다.
- 클러스터 통계보다 훨씬 더 많은 내용을 제공

## 실시간 모니터링: 인덱스 레벨

[{elasticsearch}/_stats](http://localhost:9200/_stats)

- 인덱스
  - 여러 노드에 걸친 논리적인 개념
- 노드와 비슷한 지표가 주어지지만, 물리적 노드가 아닌 인덱스를 기준으로 집계
- primaries
  - 프라이머리 샤드에 대한 정보만을 포함
- total
  - 프라이머리 / 레플리카 샤드 모두 합한 정보

# 11.4 Cat API 콘솔 모니터링

- 간단한 모니터링은 콘솔에서 수행
  - 위에서 설명해왔던 API들은 json으로 결과값 제공
  - 콘솔에서 보기에 적합하지 않음
    - 따라서 콘솔에서는 콘솔친화적인 _cat 계열 API 사용

## 목록

[{elasticsearch}/_cat](http://localhost:9200/_cat)

## 공통 파라미터

- 대다수의 리눅스 콘솔프로그램들이 `-v`, `--help`와 같은 파라미터 옵션들을 가지고 있음
  - _cat API 도 이와 유사

### v(verbose) 파라미터

[{elasticsearch}/_cat/master?v](http://localhost:9200/_cat/master?v)

- 헤더 라인도 출력해주어 각 column 이 어떤 항목인지 알기 쉬움

### help 파라미터

[{elasticsearch}/_cat/master?help](http://localhost:9200/_cat/master?help)

- 헤더의 이름이 줄임말로 쓰여있는 경우가 많은데, 이를 풀어서 설명해준다.

### h 파라미터

[{elasticsearch}/_cat/master?v&h=host,node](http://localhost:9200/_cat/master?v&h=host,node)

- 특정 열만 걸러서 보여준다

### format 파라미터

[{elasticsearch}/_cat/master?format=yaml](http://localhost:9200/_cat/master?format=yaml)

- 기본적으로 콘솔에서 보기 편한 포맷을 사용
  - 다른 포맷(text, json, smile, yaml, cbor)로 출력하기를 원하는 경우 format 파라미터 사용


## _cat API들

### [{elasticsearch}/_cat/allocation](http://localhost:9200/_cat/allocation?v)

- 노드에 할당된 샤드 수
- 디스크 정보

### [{elasticsearch}/_cat/shards](http://localhost:9200/_cat/shards?v)
[{elasticsearch}/_cat/shards/{index}](http://localhost:9200/_cat/shards/{index}?v)

- 샤드의 상태 정보
  - 샤드의 종류
  - 문서 수
  - 디스크 공간

### [{elasticsearch}/_cat/master](http://localhost:9200/_cat/master?v)

- 마스터 노드의 정보 확인

### [{elasticsearch}/_cat/nodes](http://localhost:9200/_cat/nodes?v)

- 노드에서 사용중인 fielddata 메모리 정보

### [{elasticsearch}/_cat/tasks](http://localhost:9200/_cat/tasks?v)

- 클러스터에서 동작중인 태스크

### [{elasticsearch}/_cat/indices](http://localhost:9200/_cat/indices?v)
[{elasticsearch}/_cat/indices/{index}](http://localhost:9200/_cat/indices/{index}?v)

- 루씬이 제공하는 클러스터의 인덱스 정보
- 엘라스틱서치 기준으로 문서를 세려면 _cat/count 를 사용

### [{elasticsearch}/_cat/segments](http://localhost:9200/_cat/segments?v)
[{elasticsearch}/_cat/segments/{index}](http://localhost:9200/_cat/segments/{index}?v)

- 샤드 내부의 세그먼트 정보

### [{elasticsearch}/_cat/count](http://localhost:9200/_cat/count?v)
[{elasticsearch}/_cat/count/{index}](http://localhost:9200/_cat/count/{index}?v)

- 문서 수 조회
- epoch는 unix time 나타냄

### [{elasticsearch}/_cat/recovery](http://localhost:9200/_cat/recovery?v)
[{elasticsearch}/_cat/recovery/{index}](http://localhost:9200/_cat/recovery/{index}?v)

- 현재 진행중이거나 완료된 샤드의 복구 정보
  - snapshot recovery
  - replication level 변경
  - 노드 장애 복구
  - 노드 시작 시 초기화

### [{elasticsearch}/_cat/health](http://localhost:9200/_cat/health?v)

- 클러스터 health 체크

### [{elasticsearch}/_cat/pending_tasks](http://localhost:9200/_cat/pending_tasks?v)

- 큐에 대기중인 태스크 정보

### [{elasticsearch}/_cat/aliases](http://localhost:9200/_cat/aliases?v)
[{elasticsearch}/_cat/aliases/{alias}](http://localhost:9200/_cat/aliases/{alias}?v)

- 인덱스 별칭 정보

### [{elasticsearch}/_cat/thread_pool](http://localhost:9200/_cat/thread_pool?v)
[{elasticsearch}/_cat/thread_pool/{thread_pools}](http://localhost:9200/_cat/thread_pool/{thread_pools}?v)

- 클러스터에 있는 thread pool 통계 정보
- 기본으로 보여주지 않는 정보들을 h 파라미터로 볼 수 있다.

### [{elasticsearch}/_cat/plugins](http://localhost:9200/_cat/plugins?v)

- 노드에 설치된 플러그인 정보

### [{elasticsearch}/_cat/fielddata](http://localhost:9200/_cat/fielddata?v)
[{elasticsearch}/_cat/fielddata/{fields}](http://localhost:9200/_cat/fielddata/{fields}?v)

- 클러스터가 사용중인 fielddata 캐시의 메모리 정보
- `OOM이 발생하지 않도록` 노드별로 fielddata를 항상 모니터링 해야 한다.

### [{elasticsearch}/_cat/nodeattrs](http://localhost:9200/_cat/nodeattrs?v)

- 노드를 시작할때 attribute를 지정할 수 있는데, 이를 확인하기 위한 API

### [{elasticsearch}/_cat/repositories](http://localhost:9200/_cat/repositories?v)

- 스냅샷이 저장된 저장소 정보 확인

### [{elasticsearch}/_cat/snapshots/{repository}](http://localhost:9200/_cat/snapshots/{repository}?v)

- 스냅샷 정보 확인
  - 저장소 정보를 먼저 알아야 하기 때문에 _cat/repository 를 통해서 저장소 ID를 먼저 알아야 한다.

### [{elasticsearch}/_cat/templates](http://localhost:9200/_cat/templates?v)

- 클러스터에 존재하는 템플릿 정보


# 그 밖의 것들

https://www.elastic.co/guide/en/elasticsearch/reference/master/monitor-elasticsearch-cluster.html
